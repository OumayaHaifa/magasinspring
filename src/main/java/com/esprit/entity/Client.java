package com.esprit.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "Client")
public class Client implements Serializable {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="idClient")
	private Long idClient; 
	
	private String lastname;
	
	private String firstname;
	
	private String email;
	
	private String password;
	
	@Temporal(TemporalType.DATE)
	private Date birthdate;
	
	@Enumerated(EnumType.STRING)
	private Profession profession;
	
	@Enumerated(EnumType.STRING)
	private CategorieClient categorieClient;
	
	@OneToMany(mappedBy = "client")
	private Set<Facture> factures;

	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", lastname=" + lastname + ", firstname=" + firstname + ", email="
				+ email + ", password=" + password + ", birthdate=" + birthdate + "]";
	}

	
	
}
