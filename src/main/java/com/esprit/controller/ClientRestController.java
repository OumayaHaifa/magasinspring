package com.esprit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.entity.Client;
import com.esprit.entity.Stock;
import com.esprit.services.IClientService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "Client Management")
@RestController
@RequestMapping("/client")
public class ClientRestController {
	@Autowired
	IClientService clientService;

	// http://localhost:8033/SpringMVC/client/all
	@ApiOperation(value = "Retreive all clients")
	@GetMapping("/all")
	@ResponseBody
	public List<Client> getClients() {
		List<Client> listClients = clientService.retrieveAllClients();
		return listClients;
	}
	@ApiOperation(value = "Add client")
	@PostMapping("/add")
	@ResponseBody
	public Client ajouterClient(@RequestBody Client cl)
	{
		return clientService.addClient(cl);
	}
	@ApiOperation(value = "Get client")
	@GetMapping(value = "/{idClient}")
	@ResponseBody
	public Client getClientById(@PathVariable("idClient")Long clientId) {
	return clientService.getClient(clientId);
	}

	@ApiOperation(value = "Delete client")
	@DeleteMapping("/delete/{idClient}") 
	@ResponseBody 
	public void deleteEmployeById(@PathVariable("idClient")Long clientId) {
		clientService.deleteClient(clientId);
		
	}
	
	
}