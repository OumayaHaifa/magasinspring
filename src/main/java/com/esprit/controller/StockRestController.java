package com.esprit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.esprit.entity.Stock;
import com.esprit.services.IStockServices;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api(tags = "Stock Management")
@RestController
@RequestMapping("/stock")
public class StockRestController {
	
	@Autowired
	IStockServices stockService;

//	 http://localhost:8033/SpringMVC/stock/all
	@ApiOperation(value = "Retreive stock")
	@GetMapping("/all")
	@ResponseBody
	public List<Stock> getStocks() {
		List<Stock> listStocks = stockService.getStocks();
		return listStocks;
	}
	@ApiOperation(value = "Add stock")
	@PostMapping("/add")
	@ResponseBody
	public Stock ajouterStock(@RequestBody Stock cl)
	{
		return stockService.addStock(cl);
	}
	@ApiOperation(value = "Get stock")	
	@GetMapping(value = "/{idstock}")
		@ResponseBody
		public Stock getStockById(@PathVariable("idstock")Long stockId) {
		return stockService.getStock(stockId);
	}

	@ApiOperation(value = "Delete stock")   
    @DeleteMapping("/delete/{idstock}") 
	@ResponseBody 
	public void deleteEmployeById(@PathVariable("idstock")Long stockId) {
		stockService.deleteStock(stockId);
		
	}
	    
	
	
}