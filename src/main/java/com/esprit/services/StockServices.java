package com.esprit.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.entity.Client;
import com.esprit.entity.Stock;
import com.esprit.repository.IStockRepository;

@Service
public class StockServices implements IStockServices {

	@Autowired
	IStockRepository stockRepository;
	
	private static final Logger logger = Logger.getLogger(ClientService.class);

	@Override
	public Stock addStock(Stock s) {
		Stock newStock = new Stock();
		try {

			logger.info("Starting the add stock process");

			newStock = stockRepository.save(s);

			logger.info("Ended the addStock method with success");

		} catch (Exception e) {
			logger.error("Exception thrown while adding Stock " + e);
		} finally {
			logger.info("Excited addStock method");
		}
		
		return newStock;

	}

	@Override
	public Stock getStockByLibelleStock(String libelle) {
		// TODO Auto-generated method stub
		return stockRepository.findByLibelleStock(libelle);
	}

	@Override
	public List<Stock> getStocks() {
		
		 List<Stock> myList = new ArrayList<Stock>();
		 myList= (List<Stock>) stockRepository.findAll();
		 try {

				if (myList.isEmpty()) {

					logger.info("There is no Stocks to retrieve");
				}
				else {
					for (Stock st: myList){
						logger.info(st.toString());
					}
					
				}

			} catch (Exception e) {
				logger.error("Exception thrown in getStocks" + e.getMessage());
			} finally {
				logger.info("Excited the getStocks method");
			}

			return myList;
		
	}

	@Override
	public void deleteStock(Long id) {
		try {
			logger.info("Starting the delete process");
			stockRepository.deleteById(id);
			if(stockRepository.findById(id).isPresent()) {
				logger.info("There was a problem deleting the stock ");
			}
			logger.info("Stock Deleted Successfully");
		
		}
		catch(Exception ex) {
			logger.error("Exception thrown while deleting stock" + ex.getMessage());
		}
		
		
	}

	@Override
	public Stock getStock(Long id) {
		Optional<Stock> opS;
		Stock myS = null;
		try {
			
			opS = stockRepository.findById(id);
			if(opS.isPresent()) {
				myS = opS.get();
				logger.info("Stock retreived successfully ");
			}
			else {
			logger.info("No Stock was found");
			}
		
		}
		catch(Exception ex) {
			logger.error("Exception thrown while getting stock by Id" + ex.getMessage());
		}
		return myS;
	}
}
