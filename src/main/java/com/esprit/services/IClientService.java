package com.esprit.services;

import java.util.List;

import com.esprit.entity.Client;
import com.esprit.entity.Stock;

public interface IClientService {
	
	public Client addClient(Client s);
	public List<Client> retrieveAllClients();
	public void deleteClient(Long id);
	public Client getClient(Long id); 
}
