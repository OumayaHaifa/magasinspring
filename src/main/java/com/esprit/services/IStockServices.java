package com.esprit.services;

import java.util.List;

import com.esprit.entity.Stock;

public interface IStockServices {
	
	public Stock addStock(Stock s);
	public Stock getStockByLibelleStock(String libelle);
	public List<Stock> getStocks();
	public void deleteStock(Long id);
	public Stock getStock(Long id);
}
