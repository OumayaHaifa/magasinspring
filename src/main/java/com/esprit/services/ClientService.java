package com.esprit.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.entity.Client;
import com.esprit.entity.Stock;
import com.esprit.repository.IClientRepository;

@Service
public class ClientService implements IClientService{

	@Autowired
	IClientRepository clientRepo;
	
	
	private static final Logger logger = Logger.getLogger(ClientService.class);
	
	@Override
	public Client addClient(Client s) {
		Client newC= new Client();
		try {

			logger.info("Starting the save process");

			newC = clientRepo.save(s);

			logger.info("Ended the addClient method with success");

		} catch (Exception e) {
			logger.error("Exception thrown while adding Client " + e);
		} finally {
			logger.info("Excited addClient method");
		}

		return newC;
	}

	@Override
	public List<Client> retrieveAllClients() {
		 List<Client> myList = new ArrayList();
		 myList= (List<Client>) clientRepo.findAll();
		 try {

				if (myList.isEmpty()) {

					logger.info("There is no Client to retrieve");
				}
				else {
					for (Client cl: myList){
						logger.info(cl.toString());
					}
					
				}

			} catch (Exception e) {
				logger.error("Exception thrown in retrieveAllClients" + e);
			} finally {
				logger.info("Excited the retrieveAllClients method");
			}

			return myList;
	}

	@Override
	public void deleteClient(Long id) {
		try {
			logger.info("Starting the deleteClient process");
			clientRepo.deleteById(id);
			if(clientRepo.findById(id).isPresent()) {
				logger.info("There was a problem deleting the client ");
			}
			else {
			logger.info("Client Deleted Successfully");}
		
		}
		catch(Exception ex) {
			logger.error("Exception thrown while deleting Client" + ex.getMessage());
		}
		
	}

	@Override
	public Client getClient(Long id) {
		Optional<Client> opC;
		Client myC = new Client();
		try {
			
			opC = clientRepo.findById(id);
			if(opC.isPresent()) {
				myC = opC.get();
				logger.info("Client retreived successfully ");
			}
			else {
			logger.info("No Client was found");
			}
		
		}
		catch(Exception ex) {
			logger.error("Exception thrown while getting Client by Id" + ex.getMessage());
		}
		return myC;
	}
	
	
}
