package com.esprit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.esprit.entity.Stock;

@Repository
public interface IStockRepository extends CrudRepository<Stock, Long>{

	public Stock findByLibelleStock(String libelle);
}
