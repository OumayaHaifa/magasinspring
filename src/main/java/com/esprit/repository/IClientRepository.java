package com.esprit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.esprit.entity.Client;

@Repository
public interface IClientRepository extends CrudRepository<Client, Long>{

}
