package com.esprit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableAspectJAutoProxy
@SpringBootApplication
public class MagasinApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagasinApplication.class, args);
	}

}
