package com.esprit;

import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.esprit.entity.CategorieClient;
import com.esprit.entity.Client;
import com.esprit.entity.Profession;
import com.esprit.services.ClientService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ClientServicesTest {

	@Autowired
	ClientService clientService;
	
	private static final Logger logger = Logger.getLogger(ClientServicesTest.class);
	
	@Test
	public void testAddClient() throws ParseException {
		logger.info("Inside testAddClient method");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = dateFormat.parse("1997-01-19");
		Client cl = new Client(null,"Chaarana","Omaya haifa","oumayahaifa.chaarana@esprit.tn","omaya123",d,Profession.Etudiant,CategorieClient.Ordinaire,null);
		
		
		Client newCl = clientService.addClient(cl);
				
		assertNotNull(newCl);
		logger.info("Exiting testAddClient method");
	}
	
	@Test
	public void testRetreiveClient() {
		logger.info("Inside testRetreiveClient method");
		List<Client> mylist= clientService.retrieveAllClients();	
		assertNotNull(mylist);
		logger.info("Exiting testRetreiveClient method");
	}

}
