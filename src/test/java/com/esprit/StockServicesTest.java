package com.esprit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.esprit.entity.Stock;
import com.esprit.services.StockServices;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestMethodOrder(OrderAnnotation.class)
public class StockServicesTest {

	@Autowired
	StockServices stockServices;
	
	private static final Logger logger = Logger.getLogger(StockServicesTest.class);
	
	@Test
	@Order(1)
	public void testAddStock() {
		
		Stock stock = new Stock(null,99,49,"Stock of Pasta",null);
		stockServices.addStock(stock);
		
		Stock stocked = stockServices.getStock(stock.getIdStock());
				
		assertNotNull(stocked);
		
	}
	
	@Test
	@Order(2)
	public void testDeleteStock() {
		
		Long id = 1L;
		stockServices.deleteStock(id);
		Stock deletedStock = stockServices.getStock(id);
		assertNull(deletedStock);
		
		
	}

}
